# Google Protobuf
​
Protocol Buffers (protobuf) is a data format for serialization using an interface description language (IDL), which was developed by Google and partly published under a 3-clause BSD license. Through the independent data format, all common programming languages such as C#, C++, Dart, Go, Objective-C, Java, Javascript, Python and Kotlin are officially supported. In addition, there are third-party projects that enable integration in C, Swift, Lua, Matlab, PHP, TypeScript and many more. 

Protobuf was developed in 2001 for internal use by Google and released to the public on July 7, 2008. Currently the repository is available as an open source project on Github (https://github.com/protocolbuffers/protobuf). The last stable release (v3.17.0) was published on 13.05.2021. 
​
## Why should you use Protobuf?
To motivate us for Protobuf, we don't need much at all. A simple example is enough: "Address book". 
We assume that in this address book several persons are stored, as well as the following properties of a person:
- ID
- Surname
- First name
- Email address
- Phone number

This example will be used as a basis in the following topics.
Now the question arises, how can this data be sent quickly and easily?
- C#, for example has the ability to serialize a simple object and send it. Over time, this is a very unstable approach, because the receiver must have the same memory model of the data structure at all times, as well as understand the binary formatting of C#. Backward compatibility is no longer possible here, for example. 
- Another possibility would be to create its own format and provide a small parser. In simple cases like "\<id>:\<firstname>:\<name>:\<email>:\<phonenumber>;\<id>:\<firstname>:\<name>:\<email>:\<phonenumber>;" this could work fine. But as soon as complex data structures with sub-types come into play, an enormous effort may have to be made to build the parser and maintain it accordingly. It might also be necessary to implement the parser for another programming language. That means twice the maintenance effort for at least two programming languages. 
- Another very popular exchange format is XML. It is well readable for humans and ensures a data consistency through the schema. Furthermore XML offers the advantage that it is relatively easy to read and write in all programming languages. A big disadvantage is the XML DOM. This can become very large and unclear, which in some circumstances can strongly affect the performance of the application. 
- Another and also very popular format is JSON. It is also present in almost all programming languages and easy to read for the human eye. It can be processed well up to a certain size, but at some point you will notice a loss of performance. 

### Performance Protobuf vs. JSON
Lets now compare Protobuf (binary format) and various JSON formatters.
- Google.Protobuf
- Google.Protobuf (integrated JSON formatter)
- Newtonsoft.Json
- System.Text.Json (C# from .NetCore 3.1)

The diagram below shows the serialized size (in kilobytes) for a given number of people in the address book. It can be seen very clearly that the binary format requires about half as much memory compared to the JSON formatters (the JSON was not compressed). In the lower ranges, the difference is less noteable (still about half), but as soon as the amount of data increases more significantly, the difference is very clearly visible.
​![Serialization size protobuf vs. JSON](bench_size.png)

The next image shows a comparison between Protobuf and JSON during serialization. Here, too, an address book with up to 4.000.000 people was created as a basis and then serialized with the different tools and the time was measured. It can be seen very clearly in this diagram that there is no major difference up to 100,000 objects. Only in the 7-digit range it is very clear from step to step that Protobuf for 4.000.000 persons in the address book takes a mere 3.8 seconds, while the fastest JSON serializer already takes 11.1 seconds. In percent this means that Protobuf is 294% faster than JSON.

​![Serialization time protobuf vs. JSON](bench_time.png)

In summary, it always depends on the use case. If it is known from the start that the amount of data is manageable, any method can be used. However, as soon as it is known that large amounts of data are involved, one should use a binary format. These often have an advantage in terms of performance.
##  How to create a message?

Now the question arises, how we can create such a protobuf message. Google has introduced an interface description language for this purpose, which ensures independence from a programming language.

```protobuf
syntax = "proto3";

package Example;

message Addressbook {
  repeated Person persons = 1;
}

message Person {
  int32 id = 1;
  string name = 2;
  string firstName = 3;
  string email = 4;
  repeated TelephoneNumber numbers = 5;

  message TelephoneNumber {
    string description = 1;
    string telephoneNumber = 2;
  }
}
```
In this example Protobuf version 3 (proto3) is used. In the above example, an `Addressbook` is created with a list of people. Each `person` has an `Id`, `Name`, `FirstName`, `Email` and a list of phone numbers each with a `Description` and a `TelephoneNumber`. 

At this point it is important to mention that only the data type and the names of the fields are important for the Protobuf compiler. For the serialization only the unique number (behind the "=") is important. Accordingly, it is advantageous for the performance if numbers between 1 and 15 are selected at the beginning. These only need one byte. All numbers between 16 and 2047 already need two bytes. The maximum possible number is 2^29-1 or 536.870.911. Accordingly, the larger the field number, the larger the size of the message. For those interested in more information about serialization, you can have a look at Google's guide (https://developers.google.com/protocol-buffers/docs/encoding). It describes in detail how the binary format is structured.

### Scalar data types
Protobuf provides all important data types (more info at: https://developers.google.com/protocol-buffers/docs/proto3#scalar). There it is also defined in detail which data types are mapped to in the different programming languages (C# example: **int64 => long**). 

### Enumerationen
Enum's are also implemented in Protobuf (https://developers.google.com/protocol-buffers/docs/proto3#enum). These look structurally very similar to those in common programming languages. However, there are two limitations:
- There must be a constant with the value 0, as a default numeric value.
- Additionally to remain compatible to the scheme of version 2, the constant with the value 0 must be placed at the first position.


For the serialization also only the value is of importance. The compiler uses the name to create the appropriate data structure of the respective programming language. With the naming it is important to pay attention to the styling Guide of Google Protobuf (https://developers.google.com/protocol-buffers/docs/style), otherwise it can lead e.g. to collisions with language-specific keywords. The following is an example that should be avoided at all costs.

#### Bad example

```protobuf
// Bad example
syntax = "proto3";

package EnumExample;

message EnumExampleMessage {
  ...
  enum FieldTypes {
    int = 0;
    double = 1;
    float = 2;
  }
  ...
}
```
The following generated Java code leads to a compiler error, because keywords (int, double, float) are used, which are protected by Java. In the case that you are only connecting to the interface and have no direct influence on the naming, you can use a workaround and rename the constants in the proto file. It is important that the values remain the same. However, the goal should always be to avoid such workarounds.
```java
// Generated Java example 
public enum FieldTypes implements com.google.protobuf.ProtocolMessageEnum {
    /**
    * <code>int = 0;</code>
    */
    int(0),
    /**
    * <code>double = 1;</code>
    */
    double(1),
    /**
    * <code>float = 2;</code>
    */
    float(2)
    ...
}
```
In C#, on the other hand, the above example would work.
```csharp
....
 public static partial class Types {
    public enum FieldTypes {
        [pbr::OriginalName("int")] Int = 0,
        [pbr::OriginalName("double")] Double = 1,
        [pbr::OriginalName("float")] Float = 2,
    }
}
....
```

#### Good example
It is recommended to assign a `DEFAULT` constant to the 0 value so that if no value was specified, this can be detected. 
```protobuf
// Good example
syntax = "proto3";

package EnumExample;

message EnumExampleMessage {
  ...
  enum FieldTypes {
    DEFAULT = 0;
    INT = 1;
    DOUBLE = 2;
    FLOAT = 3;
  }
  ...
}
```
This results in valid Java code.
```java
// Generated Java example 
public enum FieldTypes implements com.google.protobuf.ProtocolMessageEnum {
    /**
    * <code>DEFAULT = 0;</code>
    */
    DEFAULT(0),
    /**
    * <code>INT = 1;</code>
    */
    INT(1),
    /**
    * <code>DOUBLE = 2;</code>
    */
    DOUBLE(2),
    /**
    * <code>FLOAT = 3;</code>
    */
    FLOAT(3)
    ...
}
```
In C#, this results in more or less the same as in the bad example.
```csharp
....
public static partial class Types {
    public enum FieldTypes {
        [pbr::OriginalName("DEFAULT")] Default = 0,
        [pbr::OriginalName("INT")] Int = 1,
        [pbr::OriginalName("DOUBLE")] Double = 2,
        [pbr::OriginalName("FLOAT")] Float = 3,
    }
}
....
```

## How do you handle backward compatibility in your message format?

When it comes to backward compatibility, Protobuf is a very good example. There are only a few points to consider to ensure that messages remain backward compatible (https://developers.google.com/protocol-buffers/docs/proto3#updating). 
- The unique number of an existing field should not be changed.
- If new fields are added, they will simply be ignored by programs using the old format.  
- Existing fields can be deleted as long as the number is not reassigned. To avoid reallocation of the number, the field should be marked as `reserved`.
- Data types like int32, uint32, int64, uint64 and bool are compatible. This means that it is always possible to interchange these data types without causing compatibility problems. This is because numeric values are simply truncated if they do not fit in. As an example: If you try to store a 64-bit number in a 32-bit datatype in C++, the end is simply truncated.
- sint32 and sint64 are compatible with each other but not with other int types.
- string and bytes are compatible as long as it is valid UTF-8.

There are a few more points to consider, but they can be found in the Google Protobuf documentation (https://developers.google.com/protocol-buffers/docs/proto3#updating). 

### Simple example
Below is a simple example of backwards compatibility. I will use the address book as an example.
```protobuf
syntax = "proto3";

package BackwardsExample;

message Addressbook {
  repeated Person persons = 1;
}

message Person {
  int32 id = 1;
  string name = 2;
  string firstName = 3;
  string email = 4;
  repeated TelephoneNumber numbers = 5;

  message TelephoneNumber {
    string description = 1;
    string telephoneNumber = 2;
  }
}
```
In the sub-type `TelephoneNumber` the field `description = 1` is to be replaced by an enum.  
```protobuf
syntax = "proto3";

package BackwardsExample;

message Addressbook {
  repeated Person persons = 1;
}

message Person {
  int32 id = 1;
  string name = 2;
  string firstName = 3;
  string email = 4;
  repeated TelephoneNumber numbers = 5;

  message TelephoneNumber {
    string telephoneNumber = 2;
    NumberType numberType = 3;

    enum NumberType {
      DEFAULT = 0;
      HOME = 1;
      WORK = 2;
      MOBILE = 3;
      WORK_MOBILE = 4;
    }

    reserved 1; // mark 1 as reserved that it will never used again
  }
}
```
For example, if a service switches to a new format, the client can still communicate with the server. In this case, the information about what type of number it is is lost and accordingly it is classified as default on the server side. In case of messages from the new server format to an old client, the client will miss the description content. Alternatively, the field can be prefixed.
```protobuf
syntax = "proto3";

package BackwardsExample;

message Addressbook {
  repeated Person persons = 1;
}

message Person {
  int32 id = 1;
  string name = 2;
  string firstName = 3;
  string email = 4;
  repeated TelephoneNumber numbers = 5;

  message TelephoneNumber {
    string OBSOLETE_description = 1;
    string telephoneNumber = 2;
    NumberType numberType = 3;

    enum NumberType {
      DEFAULT = 0;
      HOME = 1;
      WORK = 2;
      MOBILE = 3;
      WORK_MOBILE = 4;
    }
  }
}
```
Accordingly, the field can be assigned a value by the server. Thus, at the server level, the name of the enum constant could be sent to the client with `.ToString()`.  


## How to debug the non-human readable format?
Debugging at the lowest level, that is at the byte level, is relatively complex and only possible for the human eye with some assistance.
In the following two methods are described. The first method is based on the byte level and is represented with the help of Base64 and a tool and offers the possibility of a structural analysis. The second approach is a bit more comfortable for the human eye. However, it is only possible after deserialization and is limited to the user data (accordingly, no structural analysis possible). 

### 1st variant - byte level (structural)
When Protobuf messages arrive at the client, they are packed into a byte array. Analyzing this data with the human eye is possible, but extremely tiring. Therefore it is advantageous to convert the byte data into Base64. For example, it can be copied from a log file and inserted into the following tool.

At the beginning we need two helper methods for the conversion into a Base64 string (example C#).
```csharp
.... 
// Helpers
public static string ByteDataToBase64(byte[] data)
{
    if (data.Length == 0)
    { // there are no Content
        return "==== Protobuf message empty ===";
    }

    return Convert.ToBase64String(data);
}

public static string ProtobufMessageToBase64(IMessage message)
{
    return ByteDataToBase64(message.ToByteArray());
}
...
```

In the following, a Protobuf message is generated and then converted to a Base64 string. In a production environment the byte data would arrive via a websocket. For simplicity, a message is first populated and then serialized. At the end of the following code snippet the Base64 string can be seen and this must now be inserted into the tool.
```csharp
...
// For example we create a message and use the binary data to analyze.
public void BinaryDataExample() {
    Addressbook addressbook = new Addressbook();
    var person = new Person
    {
        Id = 1,
        FirstName = "John",
        Name = "Doe",
        Email = "john.doe@unknown.com",
    };
    person.Numbers.Add(new Person.Types.TelephoneNumber { Description = "Work", TelephoneNumber_ = "1234/7382947" });
    addressbook.Persons.Add(person);

    var binaryData = addressbook.ToByteArray();
    // Convert to Base64
    var base64Data = ProtobufMessageToBase64(binaryData); 
    // => base64Data = CjkIARIDRG9lGgRKb2huIhRqb2huLmRvZUB1bmtub3duLmNvbSoUCgRXb3JrEgwxMjM0LzczODI5NDc= 
}
...
```

The tool is available online at https://protogen.marcgravell.com/decode. This tool supports the input format of a file, Base64 and hexa-decimal. The tool is available online but the deserialization runs completely in the browser. This means that there is no communication with the server. Accordingly, it would also be possible to evaluate sensitive data. To prevent any unexpected communication, you can go offline (developer tools in the browser). So definitely no connection with the server can be established. In the following now a picture of the "Online Protobuf Decoder". It is able to display the data hierarchically without the schema of the actual protobuf message. With all the information that the parser can read from the byte data. In comparison the corresponding schema. It can be seen very well that there are various sub-objects. In this example it is the persons or telephone numbers, where the numbers are again sub-objects of the persons. The field numbers shown in the picture (`Field #1`, `Field #2`, ...) correspond to the ones defined in the schema. 

![Serialization size protobuf vs. JSON](proto_byte_debug.png)

### 2nd variant - user data
With the second variant, you only have the possibility of structural debugging to a limited extent. This means that essentially only debugging of the user data can be performed. For this purpose, the message is parsed with the expected schema and subsequently converted to JSON. This way it can be analyzed if the content of the data matches what is expected. 

> **Important**: At this point it is important to mention that the conversion is best done only when you explicitly want to debug the messages (e.g. when the LogLevel is set to Debug). Otherwise the performance can be affected and the advantage of Protobuf is lost.

```csharp
...
void DebugData(byte[] byteData, ILogger logger) {
    // only while debug enabled or any other indicator
    if (logger.IsDebug) {
        // parse data with expected message
        Addressbook addressbook = Addressbook.Parser.ParseFrom(byteData);
        // use the built-in json formatter from Protobuf or anyone else.
        JsonFormatter formatter = new JsonFormatter(new JsonFormatter.Settings(true));
        string jsonData = formatter.Format(addressbook);
        // Log data
        logger.Debug(jsonData);
    } 
}
...
```
Ausgabe:
```json
{ 
    "persons": [ 
        { 
            "id": 1, 
            "name": "Doe", 
            "firstName": "John", 
            "email": "john.doe@unknown.com", 
            "numbers": [ 
                { 
                    "description": "Work", 
                    "telephoneNumber": "1234/7382947" 
                }
            ] 
        } 
    ] 
}
```
Here you can now look at all the data in their entirety and check for errors. 

## Summary
In conclusion, it can be said that Protobuf is a very simple and performant library for transferring data quickly and securely to the target. If one reads in the Internet various contributions to the topic, the negative aspect is often mentioned that the community is naturally not completely so large, as that of JSON. However, it must also be said that in many cases JSON is also completely sufficient. Accordingly, it is quite clear that the community is much larger. Despite all this, there are many libraries for Protobuf and also some tutorials that are, in my opinion, very informative. In addition, there is the backward compatibility, which leads to problems in many other formats, but in Google Protobuf is very easy to manage, taking into account a few points. 

## Personal opinion
I am very convinced of Protobuf and find it very easy to use. All you need to do is create a proto-message and use the corresponding compiler to create the necessary stubs for the appropriate programming language. For C# a simple NuGet is included, which handles the compilation of the proto files and is provided accordingly in the environment under the specified namespace. 

## Experiences LS/IV (from CityGis)
When connecting LS/IV, I was very confident at the beginning that a fast connection to the interface was possible. I was provided with the proto-messages and documentation. In addition, a test environment was provided by LS/IV. This means that the communication could be tested with test data without affecting a production environment. After some tests and many hours it turned out that the proto-messages and documentation did not match 100%. In some cases fields were removed and simply changed so that compatibility was not guaranteed in any case, which is why exceptions often occurred on my side because I unknowingly used the wrong message format. What made things more complicated was that CityGis used the Dutch language in the message format. Basically, I could have translated all the message fields to English and it would still have worked. The problem I have with this is the overhead of renaming which really shouldn't be necessary. Another issue that caused a problem when switching to the production environment is that in the documentation it was defined that certain fields would be transferred in all cases, but in reality there are cases where this is not true. However, this is a problem that can occur with any interface. 

## FAQ
### Why required and optional was removed in Protobuf 3?
Source: https://www.it-swarm.com.de/de/protocol-buffers/warum-erforderlich-und-optional-ist-protokollpuffer-3-entfernt/1054474949/
> The benefits of required have been at the center of many debates and flame wars. There were large camps on both sides. One camp liked to guarantee that a value was present and was willing to live with its limitations, but the other camp felt required was dangerous or unhelpful because it could not be safely added or removed.
Let me elaborate on why required fields should be used sparingly. If you are already using a proto, you cannot add a required field because old applications do not provide this field and applications generally do not handle the error well. You can make sure to update all old applications first, but it can be easy to make a mistake, and it doesn't help if you store the proto in any data store (even of short duration, such as memcached). The same is true if a required field is removed.
Many required fields were "obviously" required, until ... they were not. Suppose you have an id field for a get method. That's obviously required. Except that later you may need to change the id from int to string, or from int32 to int64. To do this, you need to add a new muchBetterId - field. Now the old id - field remains, which must be specified, but will eventually be completely ignored.
When these two problems are combined, the number of useful required fields is limited and the camps argue over whether they still have value. Opponents of required were not necessarily against the idea, but its current form. Some suggested developing a more expressive validation library that could check required along with something more advanced like name.length > 10 while ensuring a better error model.
Overall, Proto3 seems to promote simplicity, and removing required is easier. But perhaps more convincingly, removing required made sense for proto3 when combined with other features, such as removing field presence for basic elements and removing overriding default values.
I am not a Protobuf developer and in no way authoritative in this area, but I still hope the explanation is useful.

Statement from Protobuf developers:
> We deleted required fields in proto3 because required fields are generally considered harmful and violate protobuf's compatibility semantics. The idea of using protobuf is that you can add/remove fields from your protocol definition while still being fully forward/backward compatible with newer/older binaries. However, required fields will break this. You cannot safely add a required field to a .proto definition, nor can you safely remove an existing required field, as both actions break cable compatibility. For example, if you add a required field to a .proto definition, binaries created with the new definition cannot parse data serialized with the old definition because the required field is not present in old data. In a complex system where .proto definitions are shared by many different system components, adding/removing required fields can easily crash multiple parts of the system. We have seen production problems caused by this several times. In Google, it is practically forbidden to add or remove required fields everywhere. For this reason, we have completely removed required fields in proto3.
After removing "required", "optional" is just redundant, so we removed "optional" as well.


# gRPC

## What is gRPC?
gRPC (gRPC **R**emote **P**rocedure **C**all) is a protocol used to call functions in distributed computer systems (https://grpc.io/docs/what-is-grpc/introduction/). It is based on HTTP/2 and Google Protobuf. In gRPC the message definition of Protobuf was extended by a service definition. This means that a service is defined using the Protobuf 3 syntax. This applies to both server and client. Depending on which part is implemented, it must be specified whether it is server or client so that the correct stub can be generated. 

## Core concepts of gRPC
The goal of gRPC is to define a service using an interface description language (https://grpc.io/docs/what-is-grpc/core-concepts/). 

The example is taken from the official page of gRPC mentioned above.

```protobuf
service HelloService {
  rpc SayHello (HelloRequest) returns (HelloResponse);
}

message HelloRequest {
  string greeting = 1;
}

message HelloResponse {
  string reply = 1;
}
```

### Different types of service requests
#### Simple request with response (synchronous)
The client sends a request to the server and waits accordingly for the response from the serrver (synchronous).

```protobuf
rpc SayHello(HelloRequest) returns (HelloResponse);
```

#### Server streams data to client (asynchronous).
The client makes a request to the server and the server responds with a stream on which the client can then listen until the communication is terminated by the server or the connection is broken. For example, the `HelloRequest` could contain information that requests the server to provide the client with certain messages (e.g. client wants only English messages).

```protobuf
rpc LotsOfReplies(HelloRequest) returns (stream HelloResponse);
```

#### Client stream data to server (asynchronous)
The client sends a stream to the server and then can keep the stream open for an arbitrary period of time until the client finishes sending data or disconnects. 

``protobuf
rpc LotsOfGreetings(stream HelloRequest) returns (HelloResponse);
```

#### Streaming data from both sides Client <=> Server
As a fourth variant there is also the bi-directional communication. This allows both parts, server and client, to send and receive messages asynchronously. The two streams are independent of each other and messages can be exchanged at will. The order of messages is observed independently in each stream.

``protobuf
rpc BidiHello(stream HelloRequest) returns (stream HelloResponse);
```

### Timeouts/Timeout
gRPC allows clients to set a timeout for the request. That is, if a request is timed out and the server does not respond in time, then the request is terminated on the client side and an exception is thrown to the client. On the server side, however, it can be checked whether the request has already been aborted by the client or how much time remains until the time limit. 

### Metadata
Each RPC call contains metadata (such as for autentification) in the form of key-value pairs. The keys are typically strings and so are the values, although the values would also accept binary data.

### Channels
A gRPC channel provides a connection to a gRPC server. This would allow the client to change various channel settings, such as message compression. In addition, some implementations provide the status query of a channel (`Connected` or `Idle`).

## Example
The following is an example with corresponding client/server stubs (example from https://grpc.io/docs/languages/csharp/quickstart/).

### Service definition
```protobuf
// The greeting service definition.
service Greeter {
  // Sends a greeting
  rpc SayHello (HelloRequest) returns (HelloReply) {}
  // Sends another greeting
  rpc SayHelloAgain (HelloRequest) returns (HelloReply) {}
}

// The request message containing the user's name.
message HelloRequest {
  string name = 1;
}

// The response message containing the greetings
message HelloReply {
  string message = 1;
}
```

### Server implementation
In the following, `Greeter.GreeterBase` is used as the base class, which was automatically created by the compiler. 

```csharp
class GreeterImpl : Greeter.GreeterBase
{
    // Server side handler of the SayHello RPC
    public override Task<HelloReply> SayHello(HelloRequest request, ServerCallContext context)
    {
        return Task.FromResult(new HelloReply { Message = "Hello " + request.Name });
    }

    // Server side handler for the SayHelloAgain RPC
    public override Task<HelloReply> SayHelloAgain(HelloRequest request, ServerCallContext context)
    {
        return Task.FromResult(new HelloReply { Message = "Hello again " + request.Name });
    }
}
```

### Client implementation
On the client side, a stub has also been automatically generated, which is fully functional using the URL from the server. 

```csharp
public static void Main(string[] args)
{
    Channel channel = new Channel("127.0.0.1:50051", ChannelCredentials.Insecure);

    var client = new Greeter.GreeterClient(channel);
    String user = "you";

    var reply = client.SayHello(new HelloRequest { Name = user });
    Console.WriteLine("Greeting: " + reply.Message);

    var secondReply = client.SayHelloAgain(new HelloRequest { Name = user });
    Console.WriteLine("Greeting: " + secondReply.Message);

    channel.ShutdownAsync().Wait();
    Console.WriteLine("Press any key to exit...");
    Console.ReadKey();
}
```
## Why should gRPC be used?
gRPC is based on an independent description language and is therefore completely flexible to use. The official support covers all programming languages such as C#, C++, Dart, Go, Java, Kotlin, Node, Objective-C, PHP, Python and Ruby. Due to the underlying HTTP/2 protocol, it is already based on a current standard and was designed for communication within a service mesh. Horizontal scaling is possible without further ado. 