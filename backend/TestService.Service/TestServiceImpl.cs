﻿using Grpc.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestService.Messages;
using static TestService.Messages.TestServiceCommunication;

namespace TestService.Service
{
    public class TestServiceImpl : TestServiceCommunicationBase
    {
        private Dictionary<string, List<string>> _langs = new Dictionary<string, List<string>>();
        public TestServiceImpl()
        {
            _langs.Add("de", CreateLang("Hallo Welt", "Erste Nachricht", "Zweite Nachricht", "Dritte Nachricht", "Vierte Nachricht"));
            _langs.Add("en", CreateLang("Hello World", "First Message", "Second Message", "Third Message", "Fourth Message"));
            _langs.Add("nl", CreateLang("Hallo Wereld", "Eerste bericht", "Second Message", "Tweede bericht", "Vierde boodschap"));
        }

        public override async Task GetTestMessages(TestMessageRequest request, IServerStreamWriter<TestMessageResponse> responseStream, ServerCallContext context)
        {
            Random rand = new Random();
            int counter = 0;
            while (counter < request.Count && !context.CancellationToken.IsCancellationRequested)
            {
                if(_langs.TryGetValue(request.Lang, out var dataList))
                {
                    var data = dataList.ElementAtOrDefault(rand.Next(0, 4));

                    if (data != null)
                    {
                        await responseStream.WriteAsync(new TestMessageResponse
                        {
                            Description = data
                        });
                    }
                }
                counter++;
                await Task.Delay(500);
            }
        }

        private static List<string> CreateLang(params string[] text) => new List<string>(text);
    }
}
