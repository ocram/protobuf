using Google.Protobuf;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Example1
{
    [TestClass]
    public class MergeTests
    {
        [TestMethod]
        public void MergeTwoMessagesShouldWork()
        {
            FullMessage message = new FullMessage
            {
                Field1 = "1",
                Field2 = "2",
                Field3 = "3",
                Field4 = "4",
                Field5 = "5",
                Field6 = "6",
            };

            PartAMessage partA = new PartAMessage
            {
                Test1 = "1",
                Test2 = "2",
                Test3 = "3",
            };

            var bytesA = partA.ToByteArray();

            PartBMessage partB = new PartBMessage
            {
                Part1 = "4",
                Part2 = "5",
                Part3 = "6",
            };

            var bytesB = partB.ToByteArray();

            var complete = bytesA.Concat(bytesB).ToArray();

            var data = FullMessage.Parser.ParseFrom(complete);
            Assert.AreEqual(message, data);
        }
    }
}
