using Google.Protobuf;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Example1
{
    [TestClass]
    public class GeneratedTests
    {
        [TestMethod]
        public void MessageStubShouldCreated()
        {
            
        }
    }

    public static class HelperExtensions
    {
        public static string ToJson(this IMessage message)
        {
            JsonFormatter formatter = new JsonFormatter(new JsonFormatter.Settings(true));
            return formatter.Format(message);
        }
    }
}
