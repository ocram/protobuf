﻿using Google.Protobuf;
using Helper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LiveTests
{
    [TestClass]
    public class MessageDebugTests
    {
        [TestMethod]
        public void TestWithOptional()
        {
            string base64 = "CiEIARIETmFtZRoJRmlyc3RuYW1lIgx0ZXRzQHRlc3QuZGUKJAgCEgVOYW1lMhoKRmlyc3RuYW1lMiINdGV0czJAdGVzdC5kZRIIVGVzdE5hbWU=";
            Addressbook addressbook = Addressbook.Parser.ParseFrom(ProtobufHelper.Base64ToByteArray(base64));

            AddressbookInternal internalBook = new AddressbookInternal();

            if (addressbook.HasName)
            {
                internalBook.Name = addressbook.Name;
            }

            internalBook.Contacts = addressbook.Persons.Select(x =>
            {
                return new PersonInternal
                {
                    Name = x.Name,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    Id = x.Id,
                };
            }).ToArray();

            Assert.IsTrue(addressbook.HasName);
        }

        [TestMethod]
        public void TestWithoutOptional()
        {
            string base64 = "CiYIARIEVGVzdBoJRmlyc3RuYW1lIhFUZXN0RW1haWxAemVzei5kZQopCAISBVRlc3QyGgpGaXJzdG5hbWUyIhJUZXN0RW1haWwyQHplc3ouZGU=";
            Addressbook addressbook = Addressbook.Parser.ParseFrom(ProtobufHelper.Base64ToByteArray(base64));
            AddressbookInternal internalBook = new AddressbookInternal();

            if (addressbook.HasName)
            {
                internalBook.Name = addressbook.Name;
            }

            internalBook.Contacts = addressbook.Persons.Select(x =>
            {
                return new PersonInternal
                {
                    Name = x.Name,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    Id = x.Id,
                };
            }).ToArray();

            Assert.IsFalse(addressbook.HasName);
        }

        [TestMethod]
        public void TestPhonenumberType()
        {
            Addressbook addressbook = new Addressbook();
            foreach (var p in addressbook.Persons)
            {
                foreach (var n in p.Numbers)
                {
                    switch (n.TestTypeCase)
                    {
                        case Person.Types.TelephoneNumber.TestTypeOneofCase.Type:
                            // handle enum value
                            NumberType t = n.Type;
                            // ...
                            break;
                        case Person.Types.TelephoneNumber.TestTypeOneofCase.Description:
                            // handle string value
                            string description = n.Description;
                            // ...
                            break;
                    }
                }
            }
        }

        private class AddressbookInternal
        {
            public string? Name { get; set; }

            public PersonInternal[] Contacts { get; set; } = Array.Empty<PersonInternal>();
        }

        private class PersonInternal
        {
            public int Id { get; set; }

            public string Name { get; set; }

            public string FirstName { get; set;}

            public string Email { get; set; }
        }
    }
}
