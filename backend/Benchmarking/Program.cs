﻿using Google.Protobuf;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace Benchmarking
{
    class Program
    {
        static void Main(string[] args)
        {
            var counts = new[] { 1000, 10_000, 100_000, 1_000_000, 2_000_000, 3_000_000, 4_000_000 };
            foreach (int count in counts)
            {
                Console.WriteLine("**********************************************");
                Console.WriteLine("Anzahl: " + count);
                var protobufAddressbook = PrepareProtobufAddressbook(count);
                RunBenchmarkingProtobuf(protobufAddressbook);
                RunBenchmarkingProtobufJsonFormatter(protobufAddressbook);

                var jsonAddressbook = PrepareJsonAddressbook(count);
                RunBenchmarkingJson(jsonAddressbook);
                RunBenchmarkingSystemJson(jsonAddressbook);
                Console.WriteLine("**********************************************");
            }
        }

        static void RunBenchmarkingProtobuf(Addressbook addressbook)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            byte[] result = addressbook.ToByteArray();
            stopWatch.Stop();
            Console.WriteLine($"Protobuf serialization time: {stopWatch.ElapsedMilliseconds}");
            Console.WriteLine($"Protobuf serialization size (Bytes): {result.Length.ToPrettySize()}");
            Console.WriteLine();
        }

        static void RunBenchmarkingProtobufJsonFormatter(Addressbook addressbook)
        {
            JsonFormatter formatter = new JsonFormatter(new JsonFormatter.Settings(true));
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var result = formatter.Format(addressbook);
            stopWatch.Stop();

            int sizeBytes = Encoding.ASCII.GetByteCount(result);
            Console.WriteLine($"Protobuf (JSON Formatter) serialization time: {stopWatch.ElapsedMilliseconds}");
            Console.WriteLine($"Protobuf (JSON Formatter) serialization size (Bytes): {sizeBytes.ToPrettySize()}");
            Console.WriteLine();
        }

        static void RunBenchmarkingJson(AddressbookJson addressbook)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var result = JsonConvert.SerializeObject(addressbook);
            stopWatch.Stop();
            int sizeBytes = Encoding.ASCII.GetByteCount(result);
            Console.WriteLine($"JSON serialization time: {stopWatch.ElapsedMilliseconds}");
            Console.WriteLine($"JSON serialization size (Bytes): {sizeBytes.ToPrettySize()}");
            Console.WriteLine();
        }

        static void RunBenchmarkingSystemJson(AddressbookJson addressbook)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var result = System.Text.Json.JsonSerializer.Serialize(addressbook);
            stopWatch.Stop();
            int sizeBytes = Encoding.ASCII.GetByteCount(result);
            Console.WriteLine($"System JSON serialization time: {stopWatch.ElapsedMilliseconds}");
            Console.WriteLine($"System JSON serialization size (Bytes): {sizeBytes.ToPrettySize()}");
            Console.WriteLine();
        }

        static AddressbookJson PrepareJsonAddressbook(int count = 100)
        {
            AddressbookJson addressbook = new AddressbookJson();
            List<PersonJson> persons = new List<PersonJson>();
            for (int i = 0; i < count; i++)
            {
                var person = new PersonJson
                {
                    Id = i,
                    Email = $"test-{i + 1}@test.de",
                    FirstName = $"Max {i + 1}",
                    Name = $"Mustermann {i + 1}",
                };

                var numbers = new TelephoneNumberJson[]
                {
                    new TelephoneNumberJson
                    {
                        Description = "Work",
                        TelephoneNumber = "01234/5678912" + i.ToString()
                    },
                    new TelephoneNumberJson
                    {
                        Description = "Home",
                        TelephoneNumber = "98777/1234567" + i.ToString()
                    }
                };

                person.Numbers = numbers;
                persons.Add(person);
            }

            addressbook.Persons = persons.ToArray();
            return addressbook;
        }

        static Addressbook PrepareProtobufAddressbook(int count = 100)
        {
            Addressbook addressbook = new Addressbook();

            for (int i = 0; i < count; i++)
            {
                var person = new Person
                {
                    Id = i,
                    Email = $"test-{i + 1}@test.de",
                    FirstName = $"Max {i + 1}",
                    Name = $"Mustermann {i + 1}",
                };

                var numbers = new Person.Types.TelephoneNumber[]
                {
                    new Person.Types.TelephoneNumber
                    {
                        Description = "Work",
                        TelephoneNumber_ = "01234/5678912" + i.ToString()
                    },
                    new Person.Types.TelephoneNumber
                    {
                        Description = "Home",
                        TelephoneNumber_ = "98777/1234567" + i.ToString()
                    }
                };

                person.Numbers.Add(numbers);
                addressbook.Persons.Add(person);
            }

            return addressbook;
        }

    }

    public static class Ext
    {
        private const long OneKb = 1024;
        private const long OneMb = OneKb * 1024;
        private const long OneGb = OneMb * 1024;
        private const long OneTb = OneGb * 1024;

        public static string ToPrettySize(this int value, int decimalPlaces = 0)
        {
            return ((long)value).ToPrettySize(decimalPlaces);
        }

        public static string ToPrettySize(this long value, int decimalPlaces = 0)
        {
            var asTb = Math.Round((double)value / OneTb, decimalPlaces);
            var asGb = Math.Round((double)value / OneGb, decimalPlaces);
            var asMb = Math.Round((double)value / OneMb, decimalPlaces);
            var asKb = Math.Round((double)value / OneKb, decimalPlaces);
            string chosenValue = asTb > 1 ? string.Format("{0}Tb", asTb)
                : asGb > 1 ? string.Format("{0}Gb", asGb)
                : asMb > 1 ? string.Format("{0}Mb", asMb)
                : asKb > 1 ? string.Format("{0}Kb", asKb)
                : string.Format("{0}B", Math.Round((double)value, decimalPlaces));
            return chosenValue;
        }
    }
}
