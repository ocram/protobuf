﻿using System.Collections.Generic;

namespace Benchmarking
{
    internal class AddressbookJson
    {
        public IReadOnlyCollection<PersonJson> Persons { get; set; } 
    }

    internal class PersonJson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public IReadOnlyCollection<TelephoneNumberJson> Numbers { get; set; }
    }

    internal class TelephoneNumberJson
    {
        public string Description { get; set; }
        public string TelephoneNumber { get; set; }
    }
}