﻿using Google.Protobuf;
using Google.Protobuf.Reflection;
using System;

namespace Helper
{
    public static class ProtobufHelper
    {
        public static string ByteDataToBase64(byte[] data)
        {
            if (data.Length == 0)
            { // there are no Content
                return "==== Protobuf message empty ===";
            }

            return Convert.ToBase64String(data);
        }

        public static string ProtobufMessageToBase64(IMessage message)
        {
            return ByteDataToBase64(message.ToByteArray());
        }

        public static byte[] Base64ToByteArray(string messageData) => Convert.FromBase64String(messageData);

        public static T Base64ToProtobuf<T>(MessageDescriptor descriptor, string messageData)
            where T : IMessage<T>
        {
            if (string.IsNullOrEmpty(messageData))
            { // there are no Content
                throw new ArgumentNullException(nameof(messageData), "You're message should have data! It is empty.");
            }

            return (T)descriptor.Parser.ParseFrom(Convert.FromBase64String(messageData));
        }
    }
}