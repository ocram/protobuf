﻿using System;
using System.Collections.Concurrent;
using System.Linq;

namespace MessageDispatcher.Core
{
    public class MessageDispatcherService
    {
        private ConcurrentDictionary<ServiceInfo, ConcurrentQueue<MiddlewareMessage>> _messages =
            new ConcurrentDictionary<ServiceInfo, ConcurrentQueue<MiddlewareMessage>>();

        public MessageDispatcherService() { }

        public MiddlewareMessage? GetServiceMessage(ServiceInfo serviceInfo)
        {
            if (_messages.TryGetValue(serviceInfo, out var ringbuffer) && ringbuffer.TryDequeue(out var msg))
            {
                return msg;
            }

            return null;
        }

        public void PushMessage(MiddlewareMessage message)
        {
            _messages.AddOrUpdate(new ServiceInfo { ServiceName = message.ServiceName, Version = message.Version },
                addValueFactory: (ser) =>
                {
                    ConcurrentQueue<MiddlewareMessage> cq = new ConcurrentQueue<MiddlewareMessage>();
                    cq.Enqueue(message);
                    return cq;
                },
                updateValueFactory: (a, b) =>
                {
                    b.Enqueue(message);
                    Console.WriteLine("Put: " + message.Payload.ToStringUtf8());
                    return b;
                });

        }
    }
}
