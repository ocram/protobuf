﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using System.Threading.Tasks;
using static MessageDispatcher.Core.DispatchingService;

namespace MessageDispatcher.Core
{

    public class DispatchingServiceImpl : DispatchingServiceBase
    {
        private readonly MessageDispatcherService _service;

        public DispatchingServiceImpl(MessageDispatcherService service) 
        {
            _service = service;
        }

        public override async Task GetServiceMessages(ServiceInfo request, IServerStreamWriter<MiddlewareMessage> responseStream, ServerCallContext context)
        {
            while (!context.CancellationToken.IsCancellationRequested)
            {
                var data = _service.GetServiceMessage(request);

                if (data != null)
                {
                    await responseStream.WriteAsync(data);
                }
            }
        }

        public override async Task<Empty> StreamMessages(IAsyncStreamReader<MiddlewareMessage> requestStream, ServerCallContext context)
        {
            while (!context.CancellationToken.IsCancellationRequested && await requestStream.MoveNext())
            {
                var messageData = requestStream.Current;
                _service.PushMessage(messageData);
            }

            return new Empty();
        }
    }
}
