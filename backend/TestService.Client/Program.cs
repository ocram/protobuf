﻿using Google.Protobuf;
using Grpc.Net.Client;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using TestService.Messages;

namespace WeatcherTest
{
    internal class Program
    {
        private static async Task Main(string[] args)
        {
            var httpHandler = new HttpClientHandler
            {
                // Return `true` to allow certificates that are untrusted/invalid
                ServerCertificateCustomValidationCallback =
                HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
            };

            AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

            using var channel = GrpcChannel.ForAddress("https://localhost:5005", new GrpcChannelOptions { HttpHandler = httpHandler });
            var client = new TestServiceCommunication.TestServiceCommunicationClient(channel);

            try
            {
                var lang = args[0];
                var count = int.Parse(args[1]);
                CancellationToken token = new CancellationToken();
                var test = client.GetTestMessages(new TestMessageRequest { Lang = lang, Count = count });
                while (!token.IsCancellationRequested && await test.ResponseStream.MoveNext(token))
                {
                    var data = test.ResponseStream.Current;
                    Console.WriteLine($"Requested lang '{lang}': {data}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadKey();
        }
    }
}