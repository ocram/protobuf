### Create Message Live session
To make it as simple as possible for you, I would like to show it live. So that you don't have to watch me typing now, I'll speed it up a bit, but explain the important stuff. 

At the beginning we have to tell the compiler which version to use
```protobuf
syntax = "proto3";
```

In the next step we need to define a protobuf package. And because I need another C# namespace as an example, I specify it separately. If I don't specify the C# specific namespace, it would take the default one. In this example Live. 
```protobuf
package Live;
option csharp_namespace = "LiveTests";
```

The last thing we do is define our message. This contains an id, name and first name as an example.
```protobuf
message LiveTest {
  int32 id = 1;
  string name = 2;
  string firstName = 3;
}
```

Now we can save that. Compile the project and we can use our message. 

At this point it is important to mention that only the data type and the names of the fields are important for the Protobuf compiler. For the serialization only the unique number (behind the "=") is important. Accordingly, it is advantageous for the performance if numbers between 1 and 15 are selected at the beginning. These only need one byte. All numbers between 16 and 2047 already need two bytes. The maximum possible number is 536.870.911. Accordingly, the larger the field number, the larger the size of the message. For those interested in more information about serialization, you can have a look at Google's guide (https://developers.google.com/protocol-buffers/docs/encoding). It describes in detail how the binary format is structured.

> Mal zeigen wie der generierte Code aussieht

## Scalar types
Protobuf provides all important data types ( https://developers.google.com/protocol-buffers/docs/proto3#scalar). There it is also defined in detail which data types are mapped to specific types in different programming languages (C# example: int64 => long).

## Enumeration example
Enum's are also implemented in Protobuf (https://developers.google.com/protocol-buffers/docs/proto3#enum). These look structurally very similar to those in common programming languages. However, there are two limitations:

- There must be a constant with the value 0, as a default numeric value.
- Additionally to remain compatible to the scheme of version 2, the constant with the value 0 must be placed at the first position.

In the following, we add an enumeration to the example we just created. 

First we create the enum. It doesn't matter where it is placed. We can place it inside the message or separately if it is independent. 
```protobuf
enum ELiveTest {
  DEFAULT = 0;
  PLAY = 1;
  STOP = 2;
  PAUSE = 3;
}
```

Next, we just need to add another field to the message that uses the enum as its type.

```protobuf
  ELiveTest testState = 4;
```

> Mal zeigen wie der generierte Code aussieht

## How do you handle backward compatibility in your message format?
I think it's best to show directly the next topic live as well.
So after taking care of the important point of how to create such a message, I will show you how to stay backward compatible in this format. In the following example we will use the first message we created as a base. And we use the extension of the enumerations as a compatible example. I have already made a Base64 strip from our base message so we can use that in our new message.
https://protogen.marcgravell.com/decode

```csharp
// 2, Marco, Ziehm
var data = LiveTest.Parser.ParseFrom(ProtobufHelper.Base64ToByteArray("CAISBVppZWhtGgVNYXJjbw=="));
```

In this example it is of great importance that a default value is created. Otherwise another one will be assigned and possibly wrongly classified.

There are only a few points to consider to ensure that messages remain backward compatible (https://developers.google.com/protocol-buffers/docs/proto3#updating).

- The unique number of an existing field should not be changed.
- If new fields are added, they will simply be ignored by programs using the old format.
- Existing fields can be deleted as long as the number is not reassigned. To avoid reallocation of the number, the field should be marked as reserved.
- Data types like int32, uint32, int64, uint64 and bool are compatible. This means that it is always possible to interchange these data types without causing compatibility problems. This is because numeric values are simply truncated if they do not fit in. As an example: If you try to store a 64-bit number in a 32-bit datatype in C++, the end is simply truncated.
- sint32 and sint64 are compatible with each other but not with other int types.
- string and bytes are compatible as long as it is valid UTF-8.

There are a few more points to consider, but they can be found in the Google Protobuf documentation (https://developers.google.com/protocol-buffers/docs/proto3#updating).

## How to debug

Debugging at the lowest level, that is at the byte level, is very complex and only possible for the human eye with computer assistance.
I will show you two methods. The first method is based on the byte level and is represented with the help of Base64 and a tool that offers the possibility of a structural analysis. The second approach is a bit more comfortable for the human eye. However, it is only possible after deserialization and is limited to the user data (accordingly, no structural analysis possible).

### First One
When Protobuf messages arrive at the client, they are packed into a byte array. Analyzing this data with the human eye is possible, but extremely tiring. Therefore it is advantageous to convert the byte data into Base64. For example, it can be copied from a log file and inserted into the following tool.
At the beginning we need two helper methods for the conversion into a Base64 string (example C#).

```
CmkIARIDRG9lGgRKb2huIhRqb2huLmRvZUB1bmtub3duLmNvbSoUCgRXb3JrEgwxMjM0LzczODI5NDcqFQoESG9tZRINOTg3NDUvNTQ2NTE1NioXCgZNb2JpbGUSDTExMTExLzU0NDQ0NTYKaQgCEgNEb2UaBEphbmUiFGphbmUuZG9lQHVua25vd24uY29tKhQKBFdvcmsSDDExMTEvMzIxNDMyMyoVCgRIb21lEg0xMjMxMi8zMjEzMjE0KhcKBk1vYmlsZRINNTU1NS81NTU1NTU1NQ==
```

The tool is available online at https://protogen.marcgravell.com/decode. This tool supports the input format of a file, Base64 and hexa-decimal. The tool is available online but the deserialization runs completely in the browser. This means that there is no communication with the server. Accordingly, it would also be possible to evaluate sensitive data. To prevent any unexpected communication, you can go offline (developer tools in the browser). So definitely no connection with the server can be established. In the following now a picture of the "Online Protobuf Decoder". It is able to display the data hierarchically without the schema of the actual protobuf message. With all the information that the parser can read from the byte data. In comparison the corresponding schema. It can be seen very well that there are various sub-objects. In this example it is the persons or telephone numbers, where the numbers are again sub-objects of the persons. The field numbers shown in the picture (Field #1, Field #2, ...) correspond to the ones defined in the schema.

### 2nd method
With the second variant, you only have the possibility of structural debugging to a limited extent. This essentially means that only debugging of user data is possible. For this purpose, the message is parsed with the expected schema and subsequently converted to JSON. This way can be analyzed if the content of the data matches what is expected.

> Important: At this point it is important to mention that the conversion is best done only when you explicitly want to debug the messages (e.g. when the LogLevel is set to Debug). Otherwise the performance can be affected and the advantage of Protobuf is lost.

// Json Viewer
http://jsonviewer.stack.hu/

## Summary
Protobuf is a very simple, but also fast format. It takes a little discipline, but with a little practice you'll get the hang of it quickly. After reading some articles about it, you will quickly realize that in many cases JSON is preferred because it is easier to read. That's why you should discuss in advance what you need and then decide in which format. 

## Personal Opinion
I am very convinced of Protobuf and find it very easy to use. All you need to do is create a proto-message and use the corresponding compiler to create the necessary stubs for the appropriate programming language. For C# a simple NuGet is included, which handles the compilation of the proto files and is provided accordingly in the environment under the specified namespace.

## Experiences LS/IV (from CityGis)
When connecting LS/IV, I was very confident at the beginning that a fast connection to the interface was possible. I was provided with the proto-messages and documentation. In addition, a test environment was provided by LS/IV. This means that the communication could be tested with test data without affecting a production environment. After some tests and many hours it turned out that the proto-messages and documentation did not match 100%. In some cases fields were removed and simply changed so that compatibility was not guaranteed in any case, which is why exceptions often occurred on my side because I unknowingly used the wrong message format. What also made it very difficult was that CityGis used Dutch in their proto-message. Basically, I could have translated all the message fields to English and it would still have worked. The problem I have with this is the overhead of renaming which really shouldn't be necessary.

# End
I am at the end now and I hope you could gain a little insight into Protobuf and grpc.

Do you have any questions?
/// Keine Fragen?
Otherwise, I thank all participants for their attention and wish you still a nice day. 
For those who still want to try a bit, feel free to stay here. 